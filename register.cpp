#include<iostream>
#include<cstdlib>
#include<fstream>
#include<string>
#include<cstdio>
#include<map>

using namespace std;

struct info_reg
{
    int num;
    char exist;
    info_reg()
    {
        exist='n';
    }
};

typedef string mnemonic;
typedef std::map<mnemonic,info_reg>  MapType2;

MapType2 REGISTER;

void createRegisters()
{
	
REGISTER["A"].num=0;
REGISTER["A"].exist='y';

REGISTER["X"].num=1;
REGISTER["X"].exist='y';

REGISTER["L"].num=2;
REGISTER["L"].exist='y';

REGISTER["B"].num=3;
REGISTER["B"].exist='y';

REGISTER["S"].num=4;
REGISTER["S"].exist='y';

REGISTER["T"].num=5;
REGISTER["T"].exist='y';

REGISTER["F"].num=6;
REGISTER["F"].exist='y';

REGISTER["PC"].num=8;
REGISTER["PC"].exist='y';

REGISTER["SW"].num=9;
REGISTER["SW"].exist='y';
}

int reg_num(char a)
{
    if(a=='A') return 0;
    if(a=='X') return 1;
    if(a=='L') return 2;
    if(a=='B') return 3;
    if(a=='S') return 4;
    if(a=='T') return 5;
    if(a=='F') return 6;
}