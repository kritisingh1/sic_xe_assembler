#include<iostream>
#include<cstdlib>
#include<fstream>
#include<string>
#include<cstdio>
#include<map>

using namespace std;

typedef string hexa; //string literal


// Fill the remaining spaces after conversion from hexadecimal to integer with zeros.
hexa extendTo(int dig,hexa a)
{
    hexa temp="";
    for(int i=0;i<dig-a.length();++i)
        temp+='0';
    return temp+a;
}

// Convert a given hexadecimal character to corresponding integer
int toDecDig(char a)
{
    switch (a)
    {
        case '0':
            return 0;
            break;
        case '1':
            return 1;
            break;
        case '2':
            return 2;
            break;
        case '3':
            return 3;
            break;
        case '4':
            return 4;
            break;
        case '5':
            return 5;
            break;
        case '6':
            return 6;
            break;
        case '7':
            return 7;
            break;
        case '8':
            return 8;
            break;
        case '9':
            return 9;
            break;
        case 'A':
            return 10;
            break;
        case 'B':
            return 11;
            break;
        case 'C':
            return 12;
            break;
        case 'D':
            return 13;
            break;
        case 'E':
            return 14;
            break;
        case 'F':
            return 15;
            break;
    }
}

//Convert a given integer to corresponding hexadecimal character
char toHexDig(int a)
{
    switch(a)
    {
        case 0:
            return '0';
            break;
        case 1:
            return '1';
            break;
        case 2:
            return '2';
            break;
        case 3:
            return '3';
            break;
        case 4:
            return '4';
            break;
        case 5:
            return '5';
            break;
        case 6:
            return '6';
            break;
        case 7:
            return '7';
            break;
        case 8:
            return '8';
            break;
        case 9:
            return '9';
            break;
        case 10:
            return 'A';
            break;
        case 11:
            return 'B';
            break;
        case 12:
            return 'C';
            break;
        case 13:
            return 'D';
            break;
        case 14:
            return 'E';
            break;
        case 15:
            return 'F';
            break;
    }
}

string toHex(int a)
{
    string res="";
    while(a>0)
    {
        res=toHexDig(a%16)+res;
        a/=16;
    }
    return res;
}


int toDec(string val)
{
    int l=val.length()-1,curr=1,res=0;
    while(l>=0)
    {
        res+=(toDecDig(val[l])*curr);
        curr*=16;
        --l;
    }
    return res;
}

