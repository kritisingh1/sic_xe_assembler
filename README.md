AUTHOR : Kriti Singh

B.TECH CSE IInd year

IIT ROORKEE


Compile Pass2.cpp in terminal : `g++ Pass2.cpp`

Run the generated file : `./a.out`

The object program will be generated in the file named `object.txt` and the listing file will be generated in a file named `list.txt`. Errors encountered during the program, if any, are saved in `error.txt`.
